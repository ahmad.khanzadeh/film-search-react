import React from "react";
import {useEffect, useState} from 'react';
import MovieCard from "./MovieCard";
import './App.css'
import searchIcon from './search.svg'
// API key: 
// 99a7aab6

const API_URL='http://www.omdbapi.com?apikey=99a7aab6';

// const movie1={
//     "Title": "Italian Spiderman",
//     "Year": "2007",
//     "imdbID": "tt2705436",
//     "Type": "movie",
//     "Poster": "https://m.media-amazon.com/images/M/MV5BZWQxMjcwNjItZjI0ZC00ZTc4LWIwMzItM2Q0YTZhNzI3NzdlXkEyXkFqcGdeQXVyMTA0MTM5NjI2._V1_SX300.jpg"
// }
const App=() =>{
    const [movies,setMovies]=useState([])
    const [searchTerm, setSearchTerm]=useState('')

    const searchMovie= async(title)=>{
            const response=await fetch(`${API_URL}&S=${title}`);
            const data= await response.json();

            setMovies(data.Search);
            console.log(movies)
    }
    useEffect(()=>{
        searchMovie('superman');
    },[]);
    return(
       <div className="app">
            <h1>MovieLand</h1>

            <div className="search">
                <input 
                placeholder="Search for movies"
                value={searchTerm}
                onChange={(e)=>setSearchTerm(e.target.value)}
                />
                <img  
                src={searchIcon} 
                alt='search icon image'
                onClick={()=>searchMovie(searchTerm)}/>
            </div>
            {
                movies?.length > 1 ? (<div className='container'>
                                        {movies.map((movie) =>(<MovieCard movie={movie}/>))} 
                                    </div>)
                                    :(<div className="empty"> 
                                        <h2> No movies found !</h2>
                                    </div>
                                    )
            }
            
       </div>
    );
}



export default App